#!/usr/bin/python

import sys

# Create a list of the reactions with beginning of the irreversible reations and following are the reversible reactions. We should unify to CNA. Notes: regEfmtool and METATOOL are same, and both denote 0: reversible, 1: irreversible reactions. However, for METATOOL, there is a different point between regEfmtool and METATOOL, that is: the elementary flux modes matrix of METATOOL has a column header as the set of reactions. These reactions are ordered the irreversible reactions first, the reversible reactions are followed; whereas regEfmtool is contrast to.

# the reactions in a motif

# pour Vac_c
#motif = ["Vac_c","Vriso_p_Vtkx_p_Vtald_p","Vala","Tg6p","Vepi_p","Vpgi_p","Vald","Vpgi","NRJ1","Vrbco","Vcs","Vpdh","Vpepc","Vfbp","ala_up","Vtpi","Vgapdh_p","Vgapdh_Vpgk_Vpgm_Veno","Tpep"]

motif = ["Vac_m","Vriso_p_Vtkx_p_Vtald_p","Vala","Tg6p","Vepi_p","Vpgi_p","Vald","Vpgi","NRJ1","Vrbco","Vcs","Vpdh","Vpepc","Vfbp","ala_up","Vtpi","Vgapdh_p","Vgapdh_Vpgk_Vpgm_Veno","Tpep","Vaco_Vidh","NRJ2_Vkgdh_Vsdh_Vfum"]
fin = open(sys.argv[1])
efms = fin.readlines()

for i in range(len(efms)):
    efm = efms[i].strip('\n').split(' ')
    #print efm
    for m in motif:
		if m in efm:
			efm.remove(m)
    efm2s = ''
    for r in efm:
        efm2s += r + ' '
    print efm2s.strip()
        ## Apply this instruction fo cutsets.txt because its format differs
        ## from the results obtained with CNA, means: regEfmtool
        ## j = j.strip('"') 
        #count[e] += 1
        #if e != '0.0':
	#count[reactions[k]] += 1
        #k += 1
        # because this is a numeric matrix, values are real
        #if e != '0.0':
            # dung cho version cua regEfmtool
            #count[reacts[k]] += 1
        #k += 1
	
## Output
