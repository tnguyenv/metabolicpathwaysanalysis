#!/usr/bin/python 
"""
Creating a simple graph.
"""
__author__ = """Tung Nguyen Vu Ngoc (nvntung@gmail.com)"""

import sys
import networkx as nx
from networkx import *
import matplotlib.pyplot as plt
import argparse

def demo_graph_object():
	# Demo using Graph object in networkx package
	G = nx.Graph()
	G.add_node('A')
	G.add_nodes_from(['B','C'])
	
	print 'The number of nodes is',G.number_of_nodes()
	print 'The number of edges is',G.number_of_edges()
	print G.nodes()
	print G.edges()
	
	# add some edges
	G.add_edges_from([('A','B'),('A','C'),('B','C')])
	print(G.edges())
	G.add_node('D')
	G.add_edges_from([('A','D'),('B','D')])
	print(G.edges())
	#nx.draw(G)
	#plt.show()
	#plt.savefig("ABCD.png")
	# Tinh bac degree
	print(nx.degree(G))
	G.add_node('E')
	G.add_edge('C','E')
	G.remove_node('D')
	print(G.edges())
	print(nx.degree(G))
	#G=nx.erdos_renyi_graph(10,0.4)
	#nx.draw(G)
	#plt.show()
	#plt.savefig("random-graph-example.png")

	#G=nx.erdos_renyi_graph(1000,0.2)
	G=nx.grid_graph(dim=[3,4],periodic=True)
	#G=nx.grid_2d_graph(4,7,periodic=True)
	nx.draw(G)
	plt.show()
	degree_sequence=sorted(nx.degree(G).values(),reverse=True) # degree sequence
	#degree_sequence=nx.degree(G).values()
	#print "Degree sequence", degree_sequence
	dmax=max(degree_sequence)

	#z=nx.utils.create_degree_sequence(100,nx.utils.powerlaw_sequence,exponent=2.1)
	#nx.is_valid_degree_sequence(z)

	#print "Configuration model"
	#G=nx.configuration_model(z)  # configuration model
	#degree_sequence=sorted(nx.degree(G).values(),reverse=True) # degree sequence
	plt.loglog(degree_sequence,'b-',marker='o')
	#plt.title("Degree rank plot")
	plt.ylabel("number of vertices")
	plt.xlabel("degree")
	#plt.show()
	#plt.savefig("random-graph-distribution-example.png")
def main():
	demo_graph_object()

if __name__ == '__main__':
	main()
