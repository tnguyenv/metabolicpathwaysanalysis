#!/usr/bin/python

def LongestCommonSubstring(S1, S2):
    M = [[0]*(1+len(S2)) for i in xrange(1+len(S1))]
    longest, x_longest = 0, 0
    for x in xrange(1,1+len(S1)):
        for y in xrange(1,1+len(S2)):
            if S1[x-1] == S2[y-1]:
                M[x][y] = M[x-1][y-1] + 1
                if M[x][y]>longest:
                    longest = M[x][y]
                    x_longest  = x
            else:
                M[x][y] = 0
    return S1[x_longest-longest: x_longest]

s1 = ['R7i', 'R8i', 'R15', 'T1', 'T6']
s2 = ['R7i', 'R8i', 'R9', 'R10i', 'R11i', 'R12', 'R13', 'R14', 'R15', 'T5', 'T6']
#s1 = "nam and mai"
#s2 = "kenandymailenda"
print LongestCommonSubstring(s1,s2)

#from itertools import combinations, chain
#allsubsets = lambda n: list(chain(*[combinations(range(n), ni) for ni in range(n+1)]))
#mysets = allsubsets (15)
##print len(mysets)
#
#class Point:
#    def __init__(self, x, y):
#        self.x, self.y = x, y
#
#    def add(self, other):
#        return Point(self.x + other.x, self.y + other.y)
#
#    def println(self):
#        print "(%d, %d)" % (self.x, self.y)
#        
#class Motif:
#    #code
#    def __init__(self, lcs, elt1, elt2):
#        self.lcs, self.elt1, self.elt2 = lcs, elt1, elt2
#    
#    def println(self):
#        print self.lcs, "\n", self.elt1, "\n", self.elt2
#
#def filter_reactions(s):
#    efm = s.strip().split()
#    temp_efm = []
#    for e in efm:
#        if (e[0].isdigit()) or (e[0]=='-'):
#            pass
#        else:
#            temp_efm.append(e)
#        
#    return temp_efm
#
#def findsubsets(S, m):
#    return set(combinations(S, m))
#
##print findsubsets("Tung Nguyen",4)
#dd = "/home/nvntung/Dropbox/these/Analysis/MCS/krebs/"
##dd = "/home/nvntung/Dropbox/these/Analysis/MCS/muscle_new/"
#filename = "em1-list-ordered.txt"
##filename = "em-list.txt"
#fn = open(dd+filename).readlines()
#output = open(dd+"output_krebs_efm.txt","w")
#
#efmlcs = []
##for i in range(len(fn)):
##    print i
#for i in range(32700, len(mysets)-60):
#    # Moi tap tim ra chuoi dai nhat
#    print "===== class : ", i , " ======"
#    print mysets[i]
#    output.write("class : " + str(i) + "\n")
#    #In cac class ra file
#    for j in range(0,len(mysets[i])):
#        print fn[j].strip("\n")
#        output.write(fn[j].strip()+"\n")
#        s1 = filter_reactions(fn[j])
#        for k in range(j+1,len(mysets[i])):
#            print fn[k].strip("\n")
#            s2 = filter_reactions(fn[k])
#            lcs = LongestCommonSubstring(s1,s2)
#            efmlcs.append(Motif(lcs,s1,s2))
#        print "-------------"
#    print "=================="
