#!/usr/bin/python

from __future__ import division
import sys
import time
import datetime
import argparse
#from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
#                        FileTransferSpeed, FormatLabel, Percentage, \
#                        ProgressBar, ReverseBar, RotatingMarker, \
#                        SimpleProgress, Timer
import myutils

__author__ = "NGUYEN Vu Ngoc Tung (nvntung@gmail.com)"

hist_occurs_left = {}
hist_occurs_right = {}

def compare_two_files_line_by_line(file1,file2):
    file1 = open(file1).readlines()
    file2 = open(file2).readlines()
    fout = open("mlcs.lst","w")
    fmotif = open("motif.lst","w")
    id = 0
    for i in range(len(file1)):
        id += 1
        newline = ""
        l1 = myutils.filter_reactions(file1[i])
        l2 = myutils.filter_reactions(file2[i])
        mlcs = myutils.lcs(l1,l2)
        fout.write(myutils.lst2str(mlcs,' ')+"\n")
        #l1 = l1.split("\n")
        for w in l1:
            if w not in mlcs:
                newline += w + " "
                if w in hist_occurs_left:
                    hist_occurs_left[w] += 1
                else:
                    hist_occurs_left[w] = 1
        
        newline += "|| "
        #l2 = l2.split("\n")
        for w in l2:
            if w not in mlcs:
                newline += w + " "
                if w in hist_occurs_right:
                    hist_occurs_right[w] += 1
                else:
                    hist_occurs_right[w] = 1
        print newline
        fmotif.write(newline + " || " + id.__str__() + "\n")
    fout.close()
    fmotif.close()
    print "========================="
    ls_sorted = sorted(hist_occurs_left, key=hist_occurs_left.get)
    for e in ls_sorted:
        print e,":",hist_occurs_left[e]
    print "========================="
    ls_sorted = sorted(hist_occurs_right, key=hist_occurs_right.get)
    for e in ls_sorted:
        print e,":",hist_occurs_right[e]


def main():
    parser = argparse.ArgumentParser(description='Finding the different parts \
        between two files line by line, which the number of lines in both files is the same. \
        This program was written by ' +__author__+ '.',version='1.0')
    parser.add_argument('file1', help='give the name of the 1st file')
    parser.add_argument('file2', help='give the name of the 2nd file')
    
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    else:
        try:
            args = parser.parse_args()
            compare_two_files_line_by_line(args.file1,args.file2)
        except IOError, msg:
            parser.error(str(msg))

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.stdout('\nQuitting program.\n')
