#!/usr/bin/python

from __future__ import division
import sys
import time
import datetime
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
                        FileTransferSpeed, FormatLabel, Percentage, \
                        ProgressBar, ReverseBar, RotatingMarker, \
                        SimpleProgress, Timer
import subprocess

def filter_reactions(s):
    efm = s.strip().split()
    temp_efm = []
    for e in efm:
        if (e[0].isdigit()) or (e[0]=='-'):
            pass
        else:
            temp_efm.append(e)
        
    return temp_efm

#http://stackoverflow.com/q/845058/865603
def file_len1(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

#http://stackoverflow.com/a/845069/865603
def file_len2(fname):
    p = subprocess.Popen(['wc', '-l', fname], stdout=subprocess.PIPE, 
                                              stderr=subprocess.PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])

def lcs(a, b):
    #http://rosettacode.org/wiki/Longest_common_subsequence
    lengths = [[0 for j in range(len(b)+1)] for i in range(len(a)+1)]
    # row 0 and column 0 are initialized to 0 already
    for i, x in enumerate(a):
        for j, y in enumerate(b):
            if x == y:
                lengths[i+1][j+1] = lengths[i][j] + 1
            else:
                lengths[i+1][j+1] = \
                    max(lengths[i+1][j], lengths[i][j+1])
    # read the substring out from the matrix
    result = ""
    x, y = len(a), len(b)
    while x != 0 and y != 0:
        if lengths[x][y] == lengths[x-1][y]:
            x -= 1
        elif lengths[x][y] == lengths[x][y-1]:
            y -= 1
        else:
            assert a[x-1] == b[y-1]
            result = a[x-1] + " " + result
            x -= 1
            y -= 1
    return result.split()

## Tao dictionary: danh sach mcs
#global dirapps
#dirapps = "/home/nvntung/Dropbox/these/Analysis/MCS/fpc/apps/"
mcsdict = open(sys.argv[1]).readlines()

#global dout
#dout = "/media/Data/These/Analysis/MCS/Vss/"
global filename
filename = sys.argv[2]
f = open(filename)
start = time.time()
n = file_len1(filename)
end = time.time()
print ">>> Total running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))
print "The number of lines: ",n
#widgets=[SimpleProgress()]
widgets=[Percentage(), Bar()]
pbar = ProgressBar(widgets=widgets,maxval=n).start()
i = 0
j = 0
global fout
fout = open(sys.argv[3],"w")
##doan nay xu ly file: 1 cap mcs luu o hai line lien tiep, vi vay doc hai line xu ly
##for line in f:
##    nextline=f.next()
##    mt = lcs(filter_reactions(line),filter_reactions(nextline))
##    mt = mt.__str__()
##    if (mt != "[]")
##        fout.write(mt+"\n")
##    i = i + 2
##    j = j + 1
##    pbar.update(i)
##pbar.finish()
## Doan nay xu ly khi 1 couple luu bang chi muc cua mcs, ghi tren 1 dong
for line in f:
    cs = line.split('\t')
    
    mt = lcs(filter_reactions(mcsdict[int(cs[0])]),filter_reactions(mcsdict[int(cs[1])]))
    mt = mt.__str__()
    #print mt
    if mt != "[]":
        fout.write(mt+"\t"+cs[0]+"\t"+cs[1].strip("\n") + "\n")
    i = i + 1
    pbar.update(i)
pbar.finish()
end = time.time()
print ">>> Total running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))
print "The number of couples: ", i
f.close()
fout.close()
