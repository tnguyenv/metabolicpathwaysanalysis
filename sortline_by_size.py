#!/usr/bin/python

import sys
import time
import datetime
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
                        FileTransferSpeed, FormatLabel, Percentage, \
                        ProgressBar, ReverseBar, RotatingMarker, \
                        SimpleProgress, Timer
def main():
    fin = open(sys.argv[1])
    lines = fin.readlines()
    fout = open(sys.argv[2],"w")

    ## Start the procedure of counting on the running time
    start = time.time()
    mcsdict = []
    for line in lines:
        line = line.split()
        line = sorted(line, key=str.lower, reverse=False)
        #line = sorted(line, key=str.lower, reverse=True)
        mcsdict.append(line)
    
    ## Data size
    n = len(mcsdict)
    widgets=[Percentage(), Bar()]
    pbar = ProgressBar(widgets=widgets,maxval=len(mcsdict)).start()
    k = 0
    ## Bubble Sort
    i = 0
    j = 0
    tmp = []
    for i in range(0,n-1):
        for j in range(i+1,n):
            if (len(mcsdict[i])> len(mcsdict[j])):
                # swap
                tmp = mcsdict[i]
                mcsdict[i] = mcsdict[j]
                mcsdict[j] = tmp
        k = k + 1
        pbar.update(k)

    pbar.finish()

    ## Output the result into a file
    for i in range(0,n):
        s = ''
        for word in mcsdict[i]:
            s = s + ' ' + word
        s = s.strip()
        fout.write(s.strip()+"\n")
    
    ## Count the running time
    end = time.time()
    print ">>> Total running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))
    fin.close()
    fout.close()

if __name__=="__main__":                       # If this script is run as a program:
    main()
