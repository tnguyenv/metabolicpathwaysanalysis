#!/usr/bin/python 
"""
Creating a simple graph.
"""
__author__ = """Tung Nguyen Vu Ngoc (nvntung@gmail.com)"""

import sys
import networkx as nx
from networkx import *
import matplotlib.pyplot as plt
import argparse
import csv

def make_statistics_structural_properties(edgelist_graph_file,reactions_removed_file=None):
	# THIS COMPUTES SOME STRUCTURAL PROPERTIES OF THE GLOBAL NETWORK
	# Create the graph from the edgelist file
	edgelist = open(edgelist_graph_file,'r').readlines()
	G = nx.Graph()
	for e in edgelist:
		e = e.split()
		G.add_node(e[0]);
		G.add_node(e[1]);
		G.add_edge(e[0],e[1])
	# Compute the global structural properties of the global network
	print '===== Global network ====='
	print 'Number of nodes:',len(G.nodes())
	print 'Number of edges:',len(edgelist)
	print 'Connected Components: ',len(nx.connected_components(G))
	print 'Number of clusters:',len(nx.clustering(G))
	print 'Clusters:\n',nx.clustering(G)
	print 'Avg. Clustering Coefficent:',nx.average_clustering(G)
	# Print poorly the degree of the nodes
	#sorted(nx.degree(G).values())
	degrees = nx.degree(G).values()
	print 'Degree of the nodes:\n',degrees
	# Print readablely the degree of the nodes
	degrees = nx.degree(G)
	#print 'Degree of the nodes with their names:\n',degrees
	# Display beautifully the degree of the nodes
	degrees = nx.degree(G)
	temp = 0
	print "Degerees:"
	for v  in degrees:
		temp += degrees[v]
		print v,'\t',degrees[v]
	print "Avg. Deg. ", float(temp)/len(degrees)
	# Draw the full network
	#nx.draw_spring(G,node_size=0,edge_color='b',alpha=.2,font_size=10)
	nx.draw(G)
	#plt.show()
	#plt.savefig("G.png")
	
	# Compute and display the distribution of the degree
	print 'Degree histogram:\n',degree_histogram(G)
	plt.loglog(degree_histogram(G),'b-',marker='o')
	plt.title("Degree rank plot")
	plt.ylabel("degree")
	plt.xlabel("rank")
	#plt.show()
	print density(G)

	'''
	print("radius: %d" % radius(G))
	print("diameter: %d" % diameter(G))
	print("center: %s" % center(G))
	print("periphery: %s" % periphery(G))
	print("density: %s" % density(G))
	
	print "Avg. deg. ", nx.degree_assortativity_coefficient(G)
	print "Avg. Path Length ", nx.average_shortest_path_length(G)
	'''
	degcen = nx.degree_centrality(G)
	print "Degree Centrality"
	for e in degcen:
		print e," : ",degcen[e]

	betcen = nx.betweenness_centrality(G)
	print "Betweenness Centrality"
	for e in betcen:
		print e," : ",betcen[e]

	clocen = nx.closeness_centrality(G)
	print "Closeness Centrality"
	for e in clocen:
		print e," : ",clocen[e]

	ecccen = nx.eccentricity(G)
	print "Eccentricity"
	for e in ecccen:
		print e," : ",ecccen[e]
	###### Working with each of individual matrix Vac_c, Vac_f, etc.
	'''
	Using a loop to run the test for 11 matrices
	'''
	'''
	<<reactions_removed_file>> is the name of the file containing all file names of 11 matrices.
	In this folder, it is important to exist this file and 11 files containing the zero reactions.
	A zero reaction means it does not use in any EFMs. For example, Vss is one of the files.
	'''
	if reactions_removed_file == None:
		sys.exit(1)
	filenames = open(reactions_removed_file,'r').readlines()
	#res = csv.writer(open("statistic_basic_properties.csv", "wb"))
	#res.writerow(["Name","Nb.ConnectedComponents","Nb.Clusters","Density","Periphery","Diameter","Radius","Avg.Degree","Avg.ClusteringCoefficient"])
	# Extract all nodes will be deleted and all edges concerning to these nodes to a flat file
	print '\nCOMPUTE STRUCTURAL PROPERTIES OF SUB MATRICES\n'
	res = open("deleted_nodes_edges.txt","wb")
	for fn in filenames:
		print '======',fn.strip('\n'),'======'
		G3 = G.copy()
		reactions_zero = open(fn.strip('\n'),'r').readline()
		reacts = reactions_zero.split()
		# remove nodes out the started graph. These nodes are contained in, for instance, the modes_Vss_reactions_zero.txt
		# method 1: manually
		#for r in reacts:
		#	G3.remove_node(r)
		# method 2: use an available method
		G3.remove_nodes_from(reacts)
		print 'Connected Components: ',len(nx.connected_components(G3))

		# Display degrees
		degrees = nx.degree(G3)
		print 'Degree of the nodes with their names:\n',degrees
		#for deg in degrees:
		#	print deg,":",degrees[deg]
		#print(degrees)
		'''
		Removal of some nodes makes a few of nodes to be isolated. Consequently the graph is lost strong connectivity.
		So we should remove the nodes which make the graph disconnected before computing anything else.
		'''
		# Remove the nodes which degrees equal to zero
		del_nodes = []
		print 'Nb. nodes before removing: ', len(G3.nodes())
		for deg in degrees:
			if degrees[deg] == 0:
				del_nodes.append(deg)
				G3.remove_node(deg);
		print 'Nb. nodes after removing ', len(G3.nodes())
		
		# Calculate the clustering coefficient
		from networkx.algorithms import bipartite
		#print(bipartite.is_bipartite(G2))
		#print(bipartite.is_bipartite(G3))
		X, Y = bipartite.sets(G3)
		#print list(X)
		#print list(Y)
		#print 'Average clustering (the original graph):',bipartite.average_clustering(G2) 
		print 'Average clustering (the remained graph after removing some nodes):',bipartite.average_clustering(G3)
		#print '\nAverage Degree Connectivity\n',nx.average_degree_connectivity(G3)
		#print '\nNearest Neighbors\n',nx.k_nearest_neighbors(G3)
		#print '\nAverage Neighbor Degree\n',nx.average_neighbor_degree(G3)
		#print '\nDegree Centrality\n',degree_centrality(G3)
		#print '\nCloseness Centrality\n',closeness_centrality(G3)

		degree_sequence=list(degree(G3).values()) # degree sequence
		
		#print("Degree sequence %s" % degree_sequence)
		#print sum(degree_sequence)
		#print len(degree_sequence)
		#print('%f' % (sum(degree_sequence)/len(degree_sequence)))
		#print("Degree histogram")
		hist={}
		for d in degree_sequence:
			if d in hist:
				hist[d]+=1
			else:
				hist[d]=1
		#print("degree #nodes")
		#for d in hist:
		#	print('%d %d' % (d,hist[d]))
		#print density(G2)
		#print density(G3)
		'''
		print nx.eccentricity(G3)
		print nx.diameter(G3)
		print nx.radius(G3)
		'''
		print 'Connected Components: ',len(nx.connected_components(G3))
		#print sorted(nx.degree(G2).values())
		print 'Number of clusters:',len(nx.clustering(G3))
		
		G2 = G3.copy()
		#print("eccentricity: %s" % eccentricity(G))
		print("radius: %d" % radius(G2))
		print("diameter: %d" % diameter(G2))
		print("center: %s" % center(G2))
		print("periphery: %s" % periphery(G2))
		print("density: %s" % density(G2))

		# Save the results
		#res.writerow([fn,len(nx.connected_components(G3)),len(nx.clustering(G3)),density(G),periphery(G),diameter(G),radius(G),sum(degree_sequence)/len(degree_sequence),bipartite.average_clustering(G3)])

		res.write(fn)
		res.write("\t"+reacts.__str__()+"\n")
		res.write("\t"+del_nodes.__str__()+"\n")
		for line in nx.generate_edgelist(G, data=False):
			res.write("\t"+line.__str__()+"\n")
		res.write("\n\n")
	res.close()
def main():
	#use this parameter: prog='convert_matrix_to_list_gui.py', usage='%(prog)s [options]'
	parser = argparse.ArgumentParser( 
		description='This script is used to compute a few structural properties of Graph like degree, degree centrality, clustering coefficient, etc., written by '+__author__+'.',version='1.0')
	parser.add_argument('-f','--file',metavar='file-name',help='give the graph data file (.dat, .txt) containing all edges',required=True,type=file)
	parser.add_argument('-l','--list',metavar='matrix-file',help='give the name of the files containing reactions which do not use (occurrences equal to zero)',type=file,required=False)
	#parser.add_argument('-file',nargs=3,help='input 3 files: matrix-file reaction-file out-file')
	if len(sys.argv)==1:
	    parser.print_help()
	    sys.exit(1)
	try:
	    args = parser.parse_args() 
	except IOError, msg:
	    parser.error(str(msg))
	if (args.file and args.list==None):
		make_statistics_structural_properties(sys.argv[2])
	else:
		if (args.file and args.list):
			make_statistics_structural_properties(sys.argv[2],sys.argv[4])

#./networkx_demo.py -f ~/Dropbox/these/data/fpcBMC-without-subenzymes-edges.txt -l modes_zero_fnames.txt
if __name__ == '__main__':
	main()
