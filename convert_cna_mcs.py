#!/usr/bin/python

import sys
import time
import datetime
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
                        FileTransferSpeed, FormatLabel, Percentage, \
                        ProgressBar, ReverseBar, RotatingMarker, \
                        SimpleProgress, Timer
def convert_cna_mcs(source_matrix,target=None):
    widgets=[Percentage(), Bar()]
    pbar = ProgressBar(widgets=widgets, maxval=len(source_matrix)).start()
    k = 0
    bd = time.time()
    for ln in source_matrix:
        ln = ln.split()

        s = ''
        i = 0
        for i in range(len(ln)):
            '''
            if ln[i] != '0':
            if isinstance(ln[i],int)
                s += '0.0'
            else:
                s += ln[i]
            '''
            s += str(float(ln[i])) + '\t'

        s = s.strip('\t')
        s += '\n'
        if target==None:
            print(s)
        else:
            target.write(s)
            pbar.update(k)
        k += 1
    pbar.finish()
    target.close()
    kt = time.time()
    print ">>> Running time: ",kt-bd," seconds = ",str(datetime.timedelta(seconds=kt-bd))

def main():
    source_matrix = open(sys.argv[1]).readlines()
    target_list = open(sys.argv[2],"w")
    convert_cna_mcs(source_matrix,target_list)

if __name__ == '__main__':
    main()
