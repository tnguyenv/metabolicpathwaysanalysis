#!/usr/bin/python

from __future__ import division
import sys
import time
import datetime
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
                        FileTransferSpeed, FormatLabel, Percentage, \
                        ProgressBar, ReverseBar, RotatingMarker, \
                        SimpleProgress, Timer
import myutils

class Motif:
    #code
    def __init__(self, lcs, elt1, elt2):
        self.lcs, self.elt1, self.elt2 = lcs, elt1, elt2
    
    def println(self):
        print(self.lcs, "\n", self.elt1, "\n", self.elt2)
        
class SETLCS:
    #code
    def __init__(self, lcs, efm):
        self.lcs, self.efm = lcs, efm
    
    def println(self):
        print(self.lcs, "\n", self.efm)

def usage():
    print("Finding motif (common reactions) in a set of MCSs or EFMs")
    print("motiffinder <setofMCSs/EFMs> [-log [logfile]] <output_file>")
    print("<setofMCSs/EFMs>: the file containing a list of MCSs or EFMs.")
    print("<output_file>: the file containing the results.")

def main():
    global start
    start = time.time()
    global end
    end = 0
    
    print("1. Initializing data...")
    try:
        global filename
        filename = sys.argv[1]
        global mcss
        mcss = open(filename).readlines()
        global mcspairs
        mcspairs = open(sys.argv[3],"w")
        global motifs
        motifs = {}
        global groups
        groups = []
        global efmlcs
        efmlcs = {}
        print(">>> Data ready!")
        end = time.time()
        print(">>> Running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start)))
    except IOError as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))
        sys.exit()
    except ValueError:
        print("Could not convert data to an integer.")
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise
    ## Run the following step to convert decimal to binary format of EFMs and convert to list of Reactions
    lines = open(sys.argv[2]).readlines()
    reacts = lines[0]
    #print(lines[0])
    reacts = reacts.strip().split(',')
    #print(len(reacts))
    efms = []
    efmlist = open("efm-list.txt","w")
    for em in mcss:
	#em = myutils.convert_to_bin(em)
	s = ''
	em = em.strip().split()
	j = 0
	for i in em:
	    if (i !='0'):
		s += reacts[j] + ' '
	    j += 1
        efmlist.write(s+"\n")
	efms.append(s)
    efmlist.close()
    mcss = efms
    
    print("2. Find all motifs...")
    print(">>> Total lines: ", str(len(mcss)))
    cnt = 0
    #widgets = ['Processed: ', Counter(), ' lines (', Timer(), ')']
    #widgets = [FormatLabel('Processed: %(value)d lines (in: %(elapsed)s)')]
    #widgets=[SimpleProgress()]
    widgets=[Percentage(), Bar()]
    pbar = ProgressBar(widgets=widgets, maxval=len(mcss)).start()
    fPGI = open("PGI.txt","w")
    dem = 0
    for i in range(len(mcss)):
        s1 = myutils.filter_reactions(mcss[i])
        #bd = time.time()
        for j in range(i+1,len(mcss)):
            s2 = myutils.filter_reactions(mcss[j])
            mlcs = myutils.lcs(s1,s2)
            if (mlcs==['PGI']):
                dem += 1
                fPGI.write(i.__str__()+"\n"+j.__str__()+"\n")
            found = groups.count(mlcs)
            if found: # Find an existing element
                #Track of the elementary i and j have the longest common substring
                efmlcs[groups.index(mlcs)].add(i)
                efmlcs[groups.index(mlcs)].add(j)
            else: 
                # Add a new lcs into groups
                groups.append(mlcs)
                motifs[cnt] = mlcs
                efmlcs[cnt] = set([i,j])
                cnt += 1
            mcspairs.write(str(i)+"\t"+str(j)+"\n")
            mcspairs.write(mlcs.__str__() + "\n")
        #kt = time.time()
        #print ">>> Running time: ",kt-bd," seconds = ",str(datetime.timedelta(seconds=kt-bd))
        pbar.update(i)
    pbar.finish()
    fPGI.close()
    mcspairs.close()
    print("So cap: ",dem)
    # Output the result
    groups.sort()
    print("3. Reporting the results...")
    fout = open(sys.argv[4],"w")
    for e in efmlcs.keys():
        #print(e,":",motifs[e], "\n", efmlcs[e],":",len(efmlcs[e]))
        fout.write(e.__str__() + ":" + motifs[e].__str__() + "\n" + efmlcs[e].__str__() + ":" +  str(len(efmlcs[e])) + "\n")
        #print e.__str__() + ": " + motifs[e].__str__() + "\n"
        #output.write(efmlcs[e].__str__() + "\n")
        #print efmlcs[e].__str__() + "\n"
# 	   statmotif.write(motifs[e].__str__() + "\t" + str(len(efmlcs[e])) + "\t" + str(len(efmlcs[e])/numefm) + "\n")
# 	   for i in efmlcs[e]:
# 	       output.write(mcss[i])
# 	       output.write("\n")
  
    print("4. Finish.")
    #output.close()
    #statmotif.close()
    end = time.time()
    print("------------------------")
    print(">>> Total running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start)))

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.stdout('\nQuitting program.\n')
