#!/usr/bin/python

from __future__ import division
import sys
import time
import datetime
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
                        FileTransferSpeed, FormatLabel, Percentage, \
                        ProgressBar, ReverseBar, RotatingMarker, \
                        SimpleProgress, Timer
import subprocess

## http://stackoverflow.com/q/845058/865603
def file_len1(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

## File contains the motif and its list of mcs
fin = open(sys.argv[1])
start = time.time()
n = file_len1(sys.argv[1])
end = time.time()
print ">>> Total running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))
print "The number of lines: ",n
widgets=[Percentage(), Bar()]
pbar = ProgressBar(widgets=widgets,maxval=n).start()
i = 0
## File contains the motif and its size with the number of mcses
fout = open(sys.argv[2],"w")

sizehist = {'':[]}
for line in fin:
    fout.write(line)
    line = line.split(',')
    csize = len(line)
    nline = fin.next()
    nline = nline.split(',')
    l = len(nline)
    fout.write(str(csize) + "\t" + str(l)+"\n")
    # Save size histogram
    if csize in sizehist:
        sizehist[csize].add(i)
    else:
        sizehist[csize] = set()
        sizehist[csize].add(i)
    i = i + 2
    pbar.update(i)
pbar.finish()

del sizehist['']
## Thong ke so luong motif tuong ung voi tung do dai
#size = len(sizehist)
#fout = open(sys.argv[3],"w")
#for k in sizehist:
#    fout.write(str(k) + "\t" + str(len(sizehist[k])) + "\n")
#fout.close()
#print "there is ", size, " groupes"

end = time.time()
print ">>> Total running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))
print "The number of couples: ", i
fin.close()
fout.close()