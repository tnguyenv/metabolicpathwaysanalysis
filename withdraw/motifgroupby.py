#!/usr/bin/python

from __future__ import division
import sys
import time
import datetime
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
                        FileTransferSpeed, FormatLabel, Percentage, \
                        ProgressBar, ReverseBar, RotatingMarker, \
                        SimpleProgress, Timer
import subprocess
import myutils

#dout = "/media/Data/These/Analysis/MCS/Vss/"
fn = open(sys.argv[1]).readlines()

start = time.time()
n = len(fn)
end = time.time()
print ">>> Total running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))
print "The number of lines: ",n
widgets = [FormatLabel('Processed: %(value)d lines (in: %(elapsed)s)')]
#widgets=[SimpleProgress()]
#widgets=[Percentage(), Bar()]
pbar = ProgressBar(widgets=widgets,maxval=n).start()
i = 0
mcss = {'':[]}
for line in fn:
    #print line
    mt = line.split('\t')
    if mt[0] in mcss:
        #print 'Existing', mt[0]
        mcss[mt[0]].add(mt[1])
        #print mcss[mt[0]]
    else:
        #print 'Creating a new set', mt[0]
        mcss[mt[0]] = set()
        mcss[mt[0]].add(mt[1])
        #print mcss[mt[0]]
    mcss[mt[0]].add(mt[2].strip("\n"))
    #print mcss[mt[0]]
    i = i + 1
    pbar.update(i)
pbar.finish()
del mcss['']
n = len(mcss)
## Output data: motifs
widgets = [Percentage(), Bar()]
pbar = ProgressBar(widgets=widgets,maxval=n).start()
fout = open(sys.argv[2],"w")
i = 1
for key in mcss:
    ## Remove the motif contains only one reaction
    if key.find(",")>=0:
        fout.write(key + "\n")
        fout.write(mcss[key].__str__()+"\n")
    pbar.update(i)
    i = i + 1
pbar.finish()
fout.close()
print "The number of elements (motif): ", n
end = time.time()
print ">>> Total running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))

