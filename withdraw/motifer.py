#!/usr/bin/python

from __future__ import division
import sys
import time
import datetime
from itertools import combinations, chain
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
                        FileTransferSpeed, FormatLabel, Percentage, \
                        ProgressBar, ReverseBar, RotatingMarker, \
                        SimpleProgress, Timer


examples = []
def example(fn):
    try: name = 'Example %d' % int(fn.__name__[7:])
    except: name = fn.__name__

    def wrapped():
        try:
            sys.stdout.write('Running: %s\n' % name)
            fn()
            sys.stdout.write('\n')
        except KeyboardInterrupt:
            sys.stdout.write('\nSkipping example.\n\n')

    examples.append(wrapped)
    return wrapped

#@example
def example0():
    pbar = ProgressBar(widgets=[Percentage(), Bar()], maxval=300).start()
    for i in range(300):
        time.sleep(0.01)
        pbar.update(i+1)
    pbar.finish()

#@example
def example1():
    widgets = ['Test: ', Percentage(), ' ', Bar(marker=RotatingMarker()),
               ' ', ETA(), ' ', FileTransferSpeed()]
    pbar = ProgressBar(widgets=widgets, maxval=10000000).start()
    for i in range(1000000):
        # do something
        pbar.update(10*i+1)
    pbar.finish()
    
def lcs(a, b):
    #http://rosettacode.org/wiki/Longest_common_subsequence
    lengths = [[0 for j in range(len(b)+1)] for i in range(len(a)+1)]
    # row 0 and column 0 are initialized to 0 already
    for i, x in enumerate(a):
        for j, y in enumerate(b):
            if x == y:
                lengths[i+1][j+1] = lengths[i][j] + 1
            else:
                lengths[i+1][j+1] = \
                    max(lengths[i+1][j], lengths[i][j+1])
    # read the substring out from the matrix
    result = ""
    x, y = len(a), len(b)
    while x != 0 and y != 0:
        if lengths[x][y] == lengths[x-1][y]:
            x -= 1
        elif lengths[x][y] == lengths[x][y-1]:
            y -= 1
        else:
            assert a[x-1] == b[y-1]
            result = a[x-1] + " " + result
            x -= 1
            y -= 1
    return result.split()

def LongestCommonSubstring(S1, S2):
    M = [[0]*(1+len(S2)) for i in xrange(1+len(S1))]
    longest, x_longest = 0, 0
    for x in xrange(1,1+len(S1)):
        for y in xrange(1,1+len(S2)):
            if S1[x-1] == S2[y-1]:
                M[x][y] = M[x-1][y-1] + 1
                if M[x][y]>longest:
                    longest = M[x][y]
                    x_longest  = x
            else:
                M[x][y] = 0
    return S1[x_longest-longest: x_longest]

def getsubsets():
    allsubsets = lambda n: list(chain(*[combinations(range(n), ni) for ni in range(n+1)]))
    mysets = allsubsets (15)

class Point:
    def __init__(self, x, y):
        self.x, self.y = x, y

    def add(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def println(self):
        print "(%d, %d)" % (self.x, self.y)
        
class Motif:
    #code
    def __init__(self, lcs, elt1, elt2):
        self.lcs, self.elt1, self.elt2 = lcs, elt1, elt2
    
    def println(self):
        print self.lcs, "\n", self.elt1, "\n", self.elt2
        
class SETLCS:
    #code
    def __init__(self, lcs, efm):
        self.lcs, self.efm = lcs, efm
    
    def println(self):
        print self.lcs, "\n", self.efm        

def filter_reactions(s):
    efm = s.strip().split()
    temp_efm = []
    for e in efm:
        if (e[0].isdigit()) or (e[0]=='-'):
            pass
        else:
            temp_efm.append(e)
        
    return temp_efm

def findsubsets(S, m):
    return set(combinations(S, m))

def find_key(dic, val):
    """return the key of dictionary dic given the value"""
    return [k for k, v in symbol_dic.iteritems() if v == val][0]

def find_value(dic, key):
    """return the value of dictionary dic given the key"""
    return dic[key]

#@example
def example6():
    pbar = ProgressBar().start()
    for i in range(100):
        time.sleep(0.01)
        pbar.update(i + 1)
    pbar.finish()

@example
def example2():
    start = time.time()
    end = 0
    dd = "/home/nvntung/Dropbox/these/Analysis/MCS/fpc/apps/"
    #dd = "/home/nvntung/Dropbox/these/Analysis/MCS/muscle_new/"
    dout = "/home/nvntung/Dropbox/these/Analysis/MCS/fpc/statmotif/"
    filename = "mcs-Vac_f-lst.txt"
    #filename = "mcs-list-ordered.txt"
    fn = open(dd+filename).readlines()
    output = open(dout+"mcs-motif-Vac_f-detail.log","w")
    #output = open(dd+"output_muscle_efm.txt","w")
    #output = open(dd+"groups_krebs_efm_test.txt","w")
    #output2 = open(dd+"detailed_krebs_efm_test.txt","w")
    statmotif = open(dout+"mcs-motif-Vac_f.csv","w")
    motifs = {}
    groups = []
    efmlcs = {}
    cnt = 0

    #widgets = ['Processed: ', Counter(), ' lines (', Timer(), ')']
    #widgets = [FormatLabel('Processed: %(value)d lines (in: %(elapsed)s)')]
    #widgets=[SimpleProgress()]
    widgets=[Percentage(), Bar()]
    pbar = ProgressBar(widgets=widgets, maxval=len(fn)).start()
    for i in range(len(fn)):
        s1 = filter_reactions(fn[i])
        #widgets = [SimpleProgress()]
        #pbar = ProgressBar(widgets = widgets, maxval=len(fn)-i).start()
        for j in range(i+1,len(fn)):
            s2 = filter_reactions(fn[j])
            mlcs = lcs(s1,s2)
            #print "Motif: ", mlcs
            found = groups.count(mlcs)
            #print "Store: ", efmlcs
            if found: # Find an existing element
                #Track of the elementary i and j have the longest common substring
                efmlcs[groups.index(mlcs)].add(i)
                efmlcs[groups.index(mlcs)].add(j)
            else: 
                # Add a new lcs into groups
                groups.append(mlcs)
                motifs[cnt] = mlcs
                efmlcs[cnt] = set([i,j])
                cnt += 1
            #pbar.update(j)
        #pbar.finish()
        #time.sleep(0.01)
        #end = time.time()
        #print "Running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))

	pbar.update(i)
    pbar.finish()
    # Classification
    # Output the result
    #groups.sort()
    
    #numefm = len(fn)
    #
    for e in efmlcs.keys():
        print e,": ",motifs[e], "\n", efmlcs[e]
        output.write(e.__str__() + ": " + motifs[e].__str__() + "\n")
        output.write(efmlcs[e].__str__() + "\n")
        statmotif.write(motifs[e].__str__() + "\t" + str(len(efmlcs[e])) + "\t" + str(len(efmlcs[e])/numefm) + "\n")
    #    print len(efmlcs[e])/numefm
        for i in efmlcs[e]:
    #        print fn[i].strip("\n")
            output.write(fn[i])
    #    print "Total: ", len(efmlcs[e]), "\n\n"
        output.write("\n")
    output.close()
    statmotif.close()
    end = time.time()
    print "Running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))


if __name__ == '__main__':
    try:
        for example in examples: example()
    except KeyboardInterrupt:
        sys.stdout('\nQuitting examples.\n')
