#!/usr/bin/python

from __future__ import division
import sys
import os
import time
import datetime
import argparse
from itertools import *

#from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
#                        FileTransferSpeed, FormatLabel, Percentage, \
#                        ProgressBar, ReverseBar, RotatingMarker, \
#                        SimpleProgress, Timer
import myutils
__author__ = "NGUYEN Vu Ngoc Tung (nvntung@gmail.com)"

hist_occurs_left = {}
hist_occurs_right = {}

class groupby(dict):
    #s[:s.rfind(' ||')-len(s)-1]
    def __init__(self, seq, key=lambda s:s[:s.rfind(' || ')-len(s)]):
        for value in seq:
            #print value
            k = key(value)
            self.setdefault(k, []).append(value)
    __iter__ = dict.iteritems
    
def treat1():
    files = open("u-sorted-files").readlines()
    lfile = open("leftfile","w")
    rfile = open("rightfile","w")
    for f in files:
        lines = open(f.strip("\n").strip(" ")).readlines()
        for line in lines:
            line = line.split("||")
            lfile.write(line[0].strip(" ")+"\n")
            rfile.write(line[1].strip(" "))
        '''
        ll = line[0].strip("\n").split()
        for w in ll:
            if w in hist_occurs_left:
                hist_occurs_left[w] += 1
            else:
                hist_occurs_left[w] = 1
        '''
        #lr = line[1].strip("\n").split()
        '''
        for w in lr:
            if w in hist_occurs_right:
                hist_occurs_right[w] += 1
            else:
                hist_occurs_right[w] = 1
        '''
    '''    
    print "========================="
    ls_sorted = sorted(hist_occurs_left, key=hist_occurs_left.get)
    for e in ls_sorted:
        print e,":",hist_occurs_left[e]
    print "========================="
    ls_sorted = sorted(hist_occurs_right, key=hist_occurs_right.get)
    for e in ls_sorted:
        print e,":",hist_occurs_right[e]
    '''
    lfile.close()
    rfile.close()
    
def treat2(file1,file2,fout="res.txt"):
    #letters = 'abracadabra'
    #letters = ['a || 1','b || 2','r || 3','a || 4','c  || 4','a || 3','d || 2','a || 2','b || 34','r || 2','a || 2']
    #print [g for k, g in groupby(letters)]
    fout = open(fout,"w")
    lines = open(file2).readlines()
    efms = open(file1).readlines()
 
    Y = [g for k, g in groupby(lines)]
    for e in Y:
        fout.write(e[0].strip('\n')[:e[0].rfind(' || ')-len(e[0])]+'(nb. elements: '+str(len(e))+')\n')
        for e1 in e:
            t = e1.strip('\n')
            t = t[t.rfind(' || ')+4:]
            fout.write(efms[int(t)-1])
    fout.close()
            
def main():
    parser = argparse.ArgumentParser(description='Grouping motifs with the corresponding EFMs. \
        From the file with the index of EFMs, get the ordered EFMs.  \
        This program was written by ' +__author__+ '.',version='1.0')
    parser.add_argument('efms_file', help='give the name of the file containing EFMs')
    parser.add_argument('index_file', help='give the name of the file containing motifs and indexes. \
                        Each line has the form of AAA BBB || CC DD || 123')
    parser.add_argument('-o', '--output', action='store', dest='output_file', help='give the name of the output file')
    
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    else:
        try:
            args = parser.parse_args()
            if args.output_file is None:
                treat2(args.efms_file,args.index_file)
            else:
                treat2(args.efms_file,args.index_file,args.output_file)
        except IOError, msg:
            parser.error(str(msg))

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.stdout('\nQuitting program.\n')
