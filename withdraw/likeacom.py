#!/usr/bin/python

from __future__ import division
import sys
import time
import datetime
from itertools import combinations, chain


start = time.time()
end = 0

def lcs(a, b):
    #http://rosettacode.org/wiki/Longest_common_subsequence
    lengths = [[0 for j in range(len(b)+1)] for i in range(len(a)+1)]
    # row 0 and column 0 are initialized to 0 already
    for i, x in enumerate(a):
        for j, y in enumerate(b):
            if x == y:
                lengths[i+1][j+1] = lengths[i][j] + 1
            else:
                lengths[i+1][j+1] = \
                    max(lengths[i+1][j], lengths[i][j+1])
    # read the substring out from the matrix
    result = ""
    x, y = len(a), len(b)
    while x != 0 and y != 0:
        if lengths[x][y] == lengths[x-1][y]:
            x -= 1
        elif lengths[x][y] == lengths[x][y-1]:
            y -= 1
        else:
            assert a[x-1] == b[y-1]
            result = a[x-1] + " " + result
            x -= 1
            y -= 1
    return result.split()

def LongestCommonSubstring(S1, S2):
    M = [[0]*(1+len(S2)) for i in xrange(1+len(S1))]
    longest, x_longest = 0, 0
    for x in xrange(1,1+len(S1)):
        for y in xrange(1,1+len(S2)):
            if S1[x-1] == S2[y-1]:
                M[x][y] = M[x-1][y-1] + 1
                if M[x][y]>longest:
                    longest = M[x][y]
                    x_longest  = x
            else:
                M[x][y] = 0
    return S1[x_longest-longest: x_longest]

allsubsets = lambda n: list(chain(*[combinations(range(n), ni) for ni in range(n+1)]))
mysets = allsubsets (15)

class Point:
    def __init__(self, x, y):
        self.x, self.y = x, y

    def add(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def println(self):
        print "(%d, %d)" % (self.x, self.y)
        
class Motif:
    #code
    def __init__(self, lcs, elt1, elt2):
        self.lcs, self.elt1, self.elt2 = lcs, elt1, elt2
    
    def println(self):
        print self.lcs, "\n", self.elt1, "\n", self.elt2
        
class SETLCS:
    #code
    def __init__(self, lcs, efm):
        self.lcs, self.efm = lcs, efm
    
    def println(self):
        print self.lcs, "\n", self.efm        

def filter_reactions(s):
    efm = s.strip().split()
    temp_efm = []
    for e in efm:
        if (e[0].isdigit()) or (e[0]=='-'):
            pass
        else:
            temp_efm.append(e)
        
    return temp_efm

def findsubsets(S, m):
    return set(combinations(S, m))

def find_key(dic, val):
    """return the key of dictionary dic given the value"""
    return [k for k, v in symbol_dic.iteritems() if v == val][0]

def find_value(dic, key):
    """return the value of dictionary dic given the key"""
    return dic[key]

#dd = "/home/nvntung/Dropbox/these/Analysis/MCS/fpc/apps/"
#dd = "/home/nvntung/Dropbox/these/Analysis/MCS/muscle_new/"
#dout = "/home/nvntung/Dropbox/these/Analysis/MCS/fpc/statmotif/"
dd = ""
dout = ""
filename = "mcs-Vac_g-lst.txt"
#filename = "mcs-list-ordered.txt"
fn = open(dd+filename).readlines()
output = open(dout+"mcs-motif-Vac_g-detail.log","w")
#output = open(dd+"output_muscle_efm.txt","w")
#output = open(dd+"groups_krebs_efm_test.txt","w")
#output2 = open(dd+"detailed_krebs_efm_test.txt","w")
statmotif = open(dout+"mcs-motif-Vac_g.csv","w")
motifs = {}
groups = []
efmlcs = {}
cnt = 0
for i in range(len(fn)):
    s1 = filter_reactions(fn[i])
    #print "Lan lap thu: ",i
    for j in range(i+1,len(fn)):
        s2 = filter_reactions(fn[j])
        mlcs = lcs(s1,s2)
        print "Motif: ", mlcs
        found = groups.count(mlcs)
        #print "Store: ", efmlcs
        if found: # Find an existing element
            #Track of the elementary i and j have the longest common substring
            efmlcs[groups.index(mlcs)].add(i)
            efmlcs[groups.index(mlcs)].add(j)
        else: 
            # Add a new lcs into groups
            groups.append(mlcs)
            motifs[cnt] = mlcs
            efmlcs[cnt] = set([i,j])
            cnt += 1

# Output the result
#groups.sort()
numefm = len(fn)

for e in efmlcs.keys():
    print e,": ",motifs[e], "\n", efmlcs[e]
    output.write(e.__str__() + ": " + motifs[e].__str__() + "\n")
    output.write(efmlcs[e].__str__() + "\n")
    statmotif.write(motifs[e].__str__() + "\t" + str(len(efmlcs[e])) + "\t" + str(len(efmlcs[e])/numefm) + "\n")
    print len(efmlcs[e])/numefm
    for i in efmlcs[e]:
        print fn[i].strip("\n")
        output.write(fn[i])
    print "Total: ", len(efmlcs[e]), "\n\n"
    output.write("\n")
output.close()
statmotif.close()
  
# Classification

end = time.time()
print "Running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))
