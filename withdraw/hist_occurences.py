#!/usr/bin/python

import sys

## Create a list of the reactions with beginning of the irreversible reations and
## following are the reversible reactions. We should unify to CNA.
## Notes: regEfmtool and METATOOL are same, and both denote 0: reversible, 1: irreversible reactions.
## However, for METATOOL, there is a different point between regEfmtool and METATOOL,
## that is: the elementary flux modes matrix of METATOOL has a column header as the set of reactions.
## These reactions are ordered the irreversible reactions first, the reversible reactions are followed;
## whereas regEfmtool is contrast to.

## Use a dictionary to containing the result
count = {}
fin = open(sys.argv[1])
matrice = fin.readlines()
for i in range(len(matrice)):
    efm = matrice[i].rstrip('\n').split()
    k = 0
    for e in efm:
        ## Apply this instruction fo cutsets.txt because its format differs
        ## from the results obtained with CNA, means: regEfmtool
        ## j = j.strip('"') 
        e = e.strip()
        if e in count:
            count[e] += 1
        else:
            count[e] = 1
        #print e,'\t', count[e]
        #if e != '0.0':
	#count[reactions[k]] += 1
        #k += 1
        # because this is a numeric matrix, values are real
        #if e != '0.0':
            # dung cho version cua regEfmtool
            #count[reacts[k]] += 1
        #k += 1
## Output
fo = False
if (len(sys.argv) == 3):
    fout = open(sys.argv[2],"w")
    fo = True
s = ""
#ppp = sorted(count.values(),reverse=True)
for i in count:
    s = s + i + " : " + str(count[i]) + "\n"
    if fo:
        fout.write(s)
    else:
        print s.strip("\n")
    s = ""
if fo:
    fout.close()
    print("Done successfully!")

# Tally occurrences of words in a list
# import collections
# cnt = collections.Counter()
# for word in ['red', 'blue', 'red', 'green', 'blue', 'blue']:
#     cnt[word] += 1
# print(cnt)
# cnt = collections.Counter()
# for word in reacts:
#     cnt[word] += 1
# print(cnt)
#     	if      
# m=matrice[i].strip()
#     m=m.split()
#     temp=0
#     for val in m:
#         if int(val) !=0:
#             temp+=1
#     sum+=temp



#print "la moyenne des longueurs est ", float(sum)/len(matrice)


# lis=["General Store","General Store","General Store","Mall","Mall","Mall","Mall","Mall","Mall","Ice Cream Van","Ice Cream Van"]
# for x in set(lis):
#         print "{0}\n{1}".format(x,lis.count(x))
