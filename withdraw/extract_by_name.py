#!/usr/bin/python

from __future__ import division
import sys
import time
import datetime
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
                        FileTransferSpeed, FormatLabel, Percentage, \
                        ProgressBar, ReverseBar, RotatingMarker, \
                        SimpleProgress, Timer
import argparse
import myutils
__author__ = 'Nguyen Vu Ngoc Tung'

def usage():
    print "Extracting a list of EFMs or MCSs containing/not containing a reaction."
    print "Usage: extract_by_name <mcs-list-file> <rfile> <output-file> <reaction name>"
    print "- <mcs-list-file> : the file name containing EFMs or MCSs."
    print "- <rfile>: the file name containing the list of reaction names."
    print "- <output-file>: the file name containing the results."
    print "- <reaction name>: the reaction name contained in EFMs or MCSs you want to extract it."

def main():
    #use this parameter: 
    parser = argparse.ArgumentParser(#prog='extract_by_name.py', usage='%(prog)s [options]',
            description='Extracting the sub numeric matrix of the list of EFMs or MCSs containing/not containing/concerning in a certain reaction, written by '+__author__+'.',version='1.0')
    parser.add_argument('matrix',metavar='matrix-file',help='input file name (EFM matrix)')
    parser.add_argument('react',metavar='reaction-file',help='reaction file name (list of reactions)')
    parser.add_argument('fout',metavar='out-file',help='output file name')
    parser.add_argument('rname',help='reaction name')
    parser.add_argument('-i','--invert',help='invert matches for the given reaction name.')
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    try:
        args = parser.parse_args() 
    except IOError, msg:
        parser.error(str(msg))
    ## Get the source of EFMs/MCSs and the reaction list
    fin  = open(args.matrix)
    rfile = open(args.react)

    lines = fin.readlines()
    react = args.rname
    start = time.time()
    n = myutils.file_len1(args.matrix)
    
    end = time.time()
    print ">>> Loading source file in time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))
    print ">>> The number of lines: ", n
    ## Check the validity of these arguments
    ## 1. Checking the exist of the reaction name and determining its index in the file.
    reactions = rfile.readlines()[0]
    rfile.close()
    reactions = reactions.split()
    
    k = 0
    for r in reactions:
        if (r == '"'+react+'"') or (r == react):
            break 
        k += 1
    if k >= len(reactions):
        print ">>> Reaction ",react," not existing in the reaction file."
        exit()

    ## 2. Since the existance of the given reaction
    fout = open(args.fout,"w")
    widgets=[Percentage(), Bar()]
    pbar = ProgressBar(widgets=widgets,maxval=n).start()
    i = 0
    j = 0    
    cnt = 0
    
    ## Improve: -i de tim nhung line khong chua reaction name
    if args.invert:
        for line in lines:
            efm = line.split()
            if efm[k] == "0.0":
                cnt += 1
                fout.write(line)
            i += 1
            pbar.update(i)
    else:
        for line in lines:
            efm = line.split()
            if efm[k] != "0.0":
                cnt += 1
                fout.write(line)
            i += 1
            pbar.update(i)
    pbar.finish()
    print ">>> The number of EFMs containing ",react," is ", cnt
    end = time.time()
    print ">>> Total running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))
    fin.close()
    fout.close()

if __name__ == '__main__':
    main()
