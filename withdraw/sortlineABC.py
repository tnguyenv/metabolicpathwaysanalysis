#!/usr/bin/python

import sys
import time
import datetime
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
                        FileTransferSpeed, FormatLabel, Percentage, \
                        ProgressBar, ReverseBar, RotatingMarker, \
                        SimpleProgress, Timer
cnt = 0
def partition(myList, start, end):
    global cnt
    cnt = cnt + 1
    print cnt
    pivot = myList[end]
    left = start+1
    right = end
    done = False
    while not done:
        # while not done:
        #     left = left + 1
        #     if left==right:
        #         done = True
        #         break
        #     if cmp(myList[left],pivot)==1:
        #         myList[right] = myList[left]
        #         break
        # while not done:
        #     right = right - 1
        #     if left==right:
        #         done = True
        #         break
        #     if cmp(myList[right],pivot)==-1:
        #         myList[left] = myList[right]
        #         break

        while (left <= right) and (cmp(myList[left],pivot)==1):
            #print 'while 1.1'
            left = left + 1

        #print 'while 1'
        # print 'left = ',left
        # print 'right = ',right
        # print 'pivot = ',pivot
        # print cmp(myList[left],pivot)
        while (cmp(myList[right],pivot)==-1) and (right >=left):
            # print 'while 1.2'
            right = right - 1
        if right < left:
            done = True
        else:
            # swap places
            temp=myList[left]
            myList[left]=myList[right]
            myList[right]=temp
    # swap start with myList[right]
    temp=myList[start]
    myList[start]=myList[right]
    myList[right]=temp
    #myList[right]=pivot
    return right

def quicksort(myList, start, end):
    if start < end:
        # partition the list
        pivot = partition(myList, start, end)
        # sort both halves
        quicksort(myList, start, pivot-1)
        quicksort(myList, pivot+1, end)
    else:
        return

def main():
    fin = open(sys.argv[1])
    lines = fin.readlines()
    fout = open(sys.argv[2],"w")

    ## Start the procedure of counting on the running time
    start = time.time()
    mcsdict = []
    for line in lines:
        line = line.split()
        line = sorted(line, key=str.lower, reverse=False)
        #line = sorted(line, key=str.lower, reverse=True)
        mcsdict.append(line)
    
    ## Data size
    n = len(mcsdict)
    widgets=[Percentage(), Bar()]
    pbar = ProgressBar(widgets=widgets,maxval=len(mcsdict)).start()
    k = 0
    ## Bubble Sort
    i = 0
    j = 0
    tmp = []
    for i in range(0,n-1):
        for j in range(i+1,n):
            if (cmp(mcsdict[j],mcsdict[i])==-1):
                # swap
                tmp = mcsdict[i]
                mcsdict[i] = mcsdict[j]
                mcsdict[j] = tmp
        k = k + 1
        pbar.update(k)

    pbar.finish()

    ## Quick Sort
    #st = 0
    #en = n-1
    #quicksort(mcsdict,st,en)

    ## Output the result into a file
    for i in range(0,n):
        s = ''
        for word in mcsdict[i]:
            s = s + ' ' + word
        s = s.strip()
        fout.write(s.strip()+"\n")
    
    ## Count the running time
    end = time.time()
    print ">>> Total running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))
    fin.close()
    fout.close()

if __name__=="__main__":                       # If this script is run as a program:
    main()
