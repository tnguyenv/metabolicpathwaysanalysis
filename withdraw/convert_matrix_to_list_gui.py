#!/usr/bin/python
'''
Created on Oct 2, 2013

@author: nvntung
'''

import sys
import os
from PyQt4 import QtGui, QtCore
import argparse
__author__ = 'Nguyen Vu Ngoc Tung'

class Notepad(QtGui.QMainWindow):
    
    def __init__(self):
        self.source_file=''
        self.reactions = ''
        self.target_list = ''    
        super(Notepad, self).__init__()
        self.initUI()
        
    def initUI(self):
        newAction = QtGui.QAction('New', self)
        newAction.setShortcut('Ctrl+N')
        newAction.setStatusTip('Create new file')
        newAction.triggered.connect(self.newFile)
        
        saveAction = QtGui.QAction('Save', self)
        saveAction.setShortcut('Ctrl+S')
        saveAction.setStatusTip('Save current file')
        saveAction.triggered.connect(self.saveFile)
        
        openAction = QtGui.QAction('Open', self)
        openAction.setShortcut('Ctrl+O')
        openAction.setStatusTip('Open a file')
        openAction.triggered.connect(self.openFile)
        
        closeAction = QtGui.QAction('Close', self)
        closeAction.setShortcut('Ctrl+Q')
        closeAction.setStatusTip('Close Notepad')
        closeAction.triggered.connect(self.close)
        
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(newAction)
        fileMenu.addAction(saveAction)
        fileMenu.addAction(openAction)
        fileMenu.addAction(closeAction)
        
        editMenu = menubar.addMenu('&Edit')        

        page = QtGui.QWidget()        

        
        self.lbl0 = QtGui.QLabel(self)
        self.lbl0.setText('Matrix file:')
        self.edit0 = QtGui.QLineEdit()
        btnFileBrowser1 = QtGui.QPushButton('Choose file to get data...',self)
        btnFileBrowser1.clicked.connect(self.openFile)
        self.btnFileBrowser1 = btnFileBrowser1
        btnFileBrowser2 = QtGui.QPushButton('Choose a reaction file...',self)
        btnFileBrowser2.clicked.connect(self.selectFile)
        self.btnFileBrowser2 = btnFileBrowser2
        btnFileBrowser3 = QtGui.QPushButton('Choose a directory to store files...',self)
        btnFileBrowser3.clicked.connect(self.saveFile)
        self.btnFileBrowser3 = btnFileBrowser3
        
        btn = QtGui.QPushButton('Run', self)
        btn.setToolTip('This is to <b>Run</b> the program.')
        btn.resize(btn.sizeHint())
        btn.move(50, 50)
        btn.clicked.connect(self.run)
        self.button = btn

        self.lbl1 = QtGui.QLabel(self)
        self.lbl1.setText('Reaction file:')
        self.edit1 = QtGui.QLineEdit()
        self.lbl2 = QtGui.QLabel(self)
        self.lbl2.setText('Output file:')
        self.edit2 = QtGui.QLineEdit()
        
        self.text = QtGui.QTextEdit(self)


        vbox1 = QtGui.QVBoxLayout()
        vbox1.addWidget(self.lbl0)
        vbox1.addWidget(self.edit0)
        vbox1.addWidget(self.btnFileBrowser1)
        vbox1.addWidget(self.lbl1)
        vbox1.addWidget(self.edit1)
        vbox1.addWidget(self.btnFileBrowser2)
        vbox1.addWidget(self.lbl2)
        vbox1.addWidget(self.edit2)
        vbox1.addWidget(self.btnFileBrowser3)
        vbox1.addWidget(self.button)
        vbox1.addWidget(self.text)
        page.setLayout(vbox1)

        self.setCentralWidget(page)
        self.setGeometry(300,300,800,600)
        self.setWindowTitle('Convert a matrix to list')
        self.show()
        
    def run(self):
        import convert_matrix_to_list
        print(self.edit0.text)
        #source_matrix = open(self.edit0.text).readlines()
        #reactions = open(self.edit1.text).readlines()
        print(self.edit1.text)
        #reactions = reactions[0].split()
        #target_list = open(self.edit2.text,"w")
        print(self.edit2.text)
        convert_matrix_to_list.matrix2list(self.source_matrix,self.reactions,self.target_list)

    def newFile(self):
        self.text.clear()

    def saveFile(self):
        folder = QtGui.QFileDialog.getExistingDirectory(self,'Select Directory','/home/nvntung/Dropbox/these/Analysis/')

        filename = self.edit1.text
        if filename !='':
            filename = 'results.txt'
        #f = open(folder.__str__()+filename, 'w')
        #filedata = self.text.toPlainText()
        #f.write(filedata)
        self.edit2.setText(folder + '/' + filename)
        self.target_list = open(folder + '/' + filename,"w")
        #f.close()
        
    def selectFile(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, 'Choose a reaction file...', os.getenv('HOME')+'/Dropbox/these/Analysis/')
        if filename != '':
            self.edit1.setText(filename)
            print(filename)
            f = open(filename, 'r')
            filedata = f.readlines()
            self.reactions = filedata[0].split()
            #self.text.setText(filedata)
            f.close()

    def openFile(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, 'Choose a matrix file...', os.getenv('HOME')+'/Dropbox/these/Analysis/')
        if filename != '':
            self.edit0.setText(filename)
            print(filename)
            f = open(filename, 'r')
            self.source_matrix = f.readlines()
            #self.text.setText(filedata)
            f.close()

def main():
    #use this parameter: prog='convert_matrix_to_list_gui.py', usage='%(prog)s [options]'
    parser = argparse.ArgumentParser( 
            description='This script is used to convert a EFMs numerical matrix to list, written by '+__author__+'.',version='1.0')
    parser.add_argument('-g','--gui',help='enable the program using Graphic Interface',action='store_true')
    #parser.add_argument('-m','--matrix',metavar='matrix-file',help='input file name (EFM matrix)',type=file,required=False)
    #parser.add_argument('-r','--react',metavar='reaction-file',help='reaction file name (list of reactions)',type=file,required=False)
    #parser.add_argument('-l','--list',metavar='out-file',help='output file name',type=file,required=False)
    parser.add_argument('-file',nargs=3,help='input 3 files: matrix-file reaction-file out-file')
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    try:
        args = parser.parse_args() 
    except IOError, msg:
        parser.error(str(msg))
    
    if args.gui:
        print('run under graphics')
        app = QtGui.QApplication(sys.argv)
        notepad = Notepad()
        sys.exit(app.exec_())
    elif args.file:
        import convert_matrix_to_list
        source_matrix = open(sys.argv[2]).readlines()
        reactions = open(sys.argv[3]).readlines()
        reactions = reactions[0].split()
        target_list = open(sys.argv[4],"w")
        convert_matrix_to_list.matrix2list(source_matrix,reactions,target_list)
    
if __name__ == '__main__':
    main()

