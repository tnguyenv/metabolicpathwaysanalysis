#!/usr/bin/python

import subprocess

class Motif:
    #code
    def __init__(self, lcs, elt1, elt2):
        self.lcs, self.elt1, self.elt2 = lcs, elt1, elt2
    
    def println(self):
        print(self.lcs, "\n", self.elt1, "\n", self.elt2)
        
class SETLCS:
    #code
    def __init__(self, lcs, efm):
        self.lcs, self.efm = lcs, efm
    
    def println(self):
        print(self.lcs, "\n", self.efm)

def lst2str(l,delim):
    return delim.join(l)

def convert_to_bin(s):
    s = s.split()
    s1 = ''
    for i in s:
        if (i != '0'):
            s1 +='1 '
        else:
            s1 += '0 '
    return s1

def filter_reactions(s):
    efm = s.strip().split()
    temp_efm = []
    for e in efm:
        if (e[0].isdigit()): 
            temp_efm.append(e)
        elif (e[0]=='-'):
            temp_efm.append(e[1:])
        else:
            temp_efm.append(e)
        
    return temp_efm   

def combinations(S,m):
    pass

## Find combinations m elements of S
def findsubsets(S, m):
    return set(combinations(S, m))

def find_key(dic, val):
    """return the key of dictionary dic given the value"""
    return [k for k, v in symbol_dic.iteritems() if v == val][0]

def find_value(dic, key):
    """return the value of dictionary dic given the key"""
    return dic[key]


def file_len1(fname):
#http://stackoverflow.com/q/845058/865603
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def file_len2(fname):
#http://stackoverflow.com/a/845069/865603
    p = subprocess.Popen(['wc', '-l', fname], stdout=subprocess.PIPE, 
                                              stderr=subprocess.PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])

def test():
    S = [2,3,1,5,6]
    findsubsets(S,3)
    
def lcs(a, b):
#http://rosettacode.org/wiki/Longest_common_subsequence
    lengths = [[0 for j in range(len(b)+1)] for i in range(len(a)+1)]
    # row 0 and column 0 are initialized to 0 already
    for i, x in enumerate(a):
        for j, y in enumerate(b):
            if x == y:
                lengths[i+1][j+1] = lengths[i][j] + 1
            else:
                lengths[i+1][j+1] = \
                    max(lengths[i+1][j], lengths[i][j+1])
    # read the substring out from the matrix
    result = ""
    x, y = len(a), len(b)
    while x != 0 and y != 0:
        if lengths[x][y] == lengths[x-1][y]:
            x -= 1
        elif lengths[x][y] == lengths[x][y-1]:
            y -= 1
        else:
            assert a[x-1] == b[y-1]
            result = a[x-1] + " " + result
            x -= 1
            y -= 1
    return result.split()

def LongestCommonSubstring(S1, S2):
#https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Longest_common_substring#Python
    M = [[0]*(1+len(S2)) for i in xrange(1+len(S1))]
    longest, x_longest = 0, 0
    for x in xrange(1,1+len(S1)):
        for y in xrange(1,1+len(S2)):
            if S1[x-1] == S2[y-1]:
                M[x][y] = M[x-1][y-1] + 1
                if M[x][y]>longest:
                    longest = M[x][y]
                    x_longest  = x
            else:
                M[x][y] = 0
    return S1[x_longest-longest: x_longest]
