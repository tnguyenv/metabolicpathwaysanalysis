#!/usr/bin/python

from __future__ import division
import sys
import time
import datetime
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
                        FileTransferSpeed, FormatLabel, Percentage, \
                        ProgressBar, ReverseBar, RotatingMarker, \
                        SimpleProgress, Timer
import myutils

def usage():
    print("Finding motif (common reactions) in a set of MCSs or EFMs")
    print("motiffinder <setofMCSs/EFMs> [-log [logfile]] <output_file>")
    print("<setofMCSs/EFMs>: the file containing a list of MCSs or EFMs.")
    print("<output_file>: the file containing the results.")

def main():
    global start
    start = time.time()
    global end
    end = 0

    if len(sys.argv) == 1:
        exit(1)
    try:
        print "1. Initializing data..."
        global filename
        filename = sys.argv[1]
        global efms
        efms = open(filename).readlines()    
        print ">>> Data ready!"
        end = time.time()
        print ">>> Running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))

        print "2. Finding all motifs..." 
        print ">>> Total lines: ", str(len(efms))
        cnt = 0
        global mcspairs
        mcspairs = open(sys.argv[2],"w")
        global motifs
        motifs = {}
        global groups
        groups = []
        global efmlcs
        efmlcs = {}
        '''
        widgets = ['Processed: ', Counter(), ' lines (', Timer(), ')']
        widgets = [FormatLabel('Processed: %(value)d lines (in: %(elapsed)s)')]
        widgets=[SimpleProgress()]
        '''
        widgets=[Percentage(), Bar()]
        pbar = ProgressBar(widgets=widgets, maxval=len(efms)).start()
        start = time.time()
        for i in range(len(efms)):
            s1 = myutils.filter_reactions(efms[i])
            for j in range(i+1,len(efms)):
                s2 = myutils.filter_reactions(efms[j])
                mlcs = myutils.lcs(s1,s2)
                '''
                print s1
                print s2
                print mlcs
                print "\n"
                '''
                found = groups.count(mlcs)
                if found: # Find an existing element
                #Track of the elementary i and j have the longest common substring
                    efmlcs[groups.index(mlcs)].add(i)
                    efmlcs[groups.index(mlcs)].add(j)
                else: 
                    # Add a new lcs into groups
                    groups.append(mlcs)
                    motifs[cnt] = mlcs
                    efmlcs[cnt] = set([i,j])
                    cnt += 1
                    #mcspairs.write(str(i)+"\t"+str(j)+"\n")
                    #mcspairs.write(mlcs.__str__() + "\n")
                pbar.update(i)
        pbar.finish()
        for i in range(len(groups)):
            if len(groups[i]) > 0:
                s = ""
                for j in groups[i]:
                    s += j + " "
                mcspairs.write(s + "\n")
                s = ""
                for j in efmlcs[i]:
                    s += j.__str__() + " "
                mcspairs.write(s + "\n")
        mcspairs.close()
        end = time.time()
        print ">>> Running time: ",end-start," seconds = ",str(datetime.timedelta(seconds=end-start))
    except IOError as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))
        sys.exit()
    except ValueError:
        print("Could not convert data to an integer.")
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise
    end = time.time()
    print "------------------------"
    print ">>> Total running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))
    
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.stdout('\nQuitting program.\n')
