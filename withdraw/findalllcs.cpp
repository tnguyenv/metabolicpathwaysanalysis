//http://www.siafoo.net/snippet/380
#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
 
int main (int argc, char * const argv[]) {
  std::cout << "The purpose is to find the longest and NOT all of the "
    "actual subsequences.  This algorithm is a brute force algorithm to "
    "demonstrate finding the longest common subsequence." << std::endl
	    << std::endl
	    << "First we proceed by going through X compared to Y and then "
    "going through comparing Y to X.  In either case we get 'a' not 'the' "
    "longest subsequence." << std::endl << std::endl;
  std::vector<std::string> vs;
  std::string x="ABCDBAGZCDCA";
  std::string y="BCADCGZ";
  //std::string y="bcadcgz";
  //std::string x="ABCDE";
  //std::string y="abcde";
  size_t n=x.size();
  size_t m=y.size();
  size_t i(0), j(0), ii(0), jj(0);
  std::string ts;
  std::string longest;
  size_t count=0;
  for (; i < n; ++i ) {
    ii = i;
    j = 0;
    jj = j;
    ts.clear();
    do {
      count++;
      if (x[ii] == y[j]) {
	std::cout << x[ii];
	ts += x[ii];
	if ( ii == n-1 && j == 0) break;
	++ii;
	jj=++j;
      } else if ( ++j == m ) {
	j = jj;
	// wasn't found so next in X
	++ii;
	if ( j == 0 && ii == n ) break;
      }
			
    } while ( ii < n && jj < m );
    std::cout << std::endl;
    if (ts.size() > longest.size()) longest = ts;
    vs.push_back(ts);
    if ( j == 0 && ii >= n-1 ) break;
  }
  std::cout << "Actual number of comparisons/looping: " << count << std::endl << std::endl;
  std::cout << "Longest is: " << longest << std::endl << std::endl;
  std::cout << std::endl << "Going the other way!" << std::endl;
  j=0;
  i=0;
  jj=0;
  ii=0;
  longest.clear();
  count=0;
  for (; i < m; ++i ) {
    ii = i;
    j = 0;
    jj = j;
    ts.clear();
    do {
      count++;
      if (y[ii] == x[j]) {
	std::cout << y[ii];
	ts+=y[ii];
	if ( ii == n-1 && j == 0) break;
	++ii;
	jj=++j;
      } else if ( ++j == n ) {
	j = jj;
	// wasn't found so next in Y
	++ii;
	if ( j == 0 && ii == n) break;
      }
			
    } while ( ii < m && jj < n );
    std::cout << std::endl;
    vs.push_back(ts);
    if (ts.size() > longest.size()) longest = ts;
    if ( j == 0 && ii >= n-1 ) break;
  }
  std::cout << "Actual number of comparisons/looping: " << count << std::endl << std::endl;
  std::cout << "Longest is: " << longest << std::endl << std::endl;
  std::cout << " O(n*m) = O(" << n*m << ")" << std::endl; 
  std::cout << "UNIQUE SORTED ORDER: " << std::endl;
	
  std::sort(vs.begin(), vs.end());
  std::vector<std::string>::iterator it = std::unique_copy (vs.begin(), vs.end(), vs.begin());
  vs.resize(it - vs.begin());
  for (i=0;i<vs.size();++i) {
    std::cout << vs[i] << std::endl;
  }
  return 0;
}
