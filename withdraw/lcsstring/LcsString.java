
public class LcsString {

	public static String lcs(String a, String b) {
		int[][] lengths = new int[a.length()+1][b.length()+1];

		// row 0 and column 0 are initialized to 0 already

		for (int i = 0; i < a.length(); i++)
			for (int j = 0; j < b.length(); j++)
				if (a.charAt(i) == b.charAt(j))
					lengths[i+1][j+1] = lengths[i][j] + 1;
				else
					lengths[i+1][j+1] =
					Math.max(lengths[i+1][j], lengths[i][j+1]);

		// read the substring out from the matrix
		StringBuffer sb = new StringBuffer();
		for (int x = a.length(), y = b.length();
				x != 0 && y != 0; ) {
			if (lengths[x][y] == lengths[x-1][y])
				x--;
			else if (lengths[x][y] == lengths[x][y-1])
				y--;
			else {
				assert a.charAt(x-1) == b.charAt(y-1);
				sb.append(a.charAt(x-1));
				x--;
				y--;
			}
		}

		return sb.reverse().toString();
	}

	public static String recurlcs(String a, String b){
		int aLen = a.length();
		int bLen = b.length();
		if(aLen == 0 || bLen == 0){
			return "";
		}else if(a.charAt(aLen-1) == b.charAt(bLen-1)){
			return lcs(a.substring(0,aLen-1),b.substring(0,bLen-1))
					+ a.charAt(aLen-1);
		}else{
			String x = lcs(a, b.substring(0,bLen-1));
			String y = lcs(a.substring(0,aLen-1), b);
			return (x.length() > y.length()) ? x : y;
		}
	}

	private static String cmp(String a, String b) {
        // TODO Auto-generated method stub
        String rs ="";
        String[] as = a.split(" ");
        String[] bs = b.split(" ");

        int idx =0;
        while (idx < as.length || idx < bs.length){
            if (as[idx].equals(bs[idx])) {
                rs += as[idx] + " ";
            } else {
            	break;
            }
            idx++;
        }
        
        return rs;
    }
	
	// EXAMPLE.  Here's how you use it.  
	public static void main(String[] args) {
		String a = "R10i R14 R15 R6i ATP"; // "<p>the quick brown fox</p>";
		String b = "R11i R6i R7i T2"; // "<p>the <b>Fast</b> brown dog</p>";

		//LcsString seq = new LcsString("<p>the quick brown fox</p>","<p>the <b>Fast</b> brown dog</p>");
		//System.out.println("LCS: "+seq.getLcsLength());
		//System.out.println("Edit Dist: "+seq.getMinEditDistance());
		//System.out.println("Backtrack: "+seq.backtrack());
		//System.out.println("HTML Diff: "+seq.getHtmlDiff());
		System.out.println(cmp(a,b));
	}
}