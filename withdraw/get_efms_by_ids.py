#!/usr/bin/python
'''
Lay cac EFMs dua tren index cua chung
Example:
./get_efm_by_id 2 3 12 23
'''

from __future__ import division
import sys
import os
import time
import datetime
import argparse
from sets import Set
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
                        FileTransferSpeed, FormatLabel, Percentage, \
                        ProgressBar, ReverseBar, RotatingMarker, \
                        SimpleProgress, Timer
import myutils
__author__ = "NGUYEN Vu Ngoc Tung (nvntung@gmail.com)"

def print_efms_via_ids(inputfile,indices=range(0,10)):
    efms = open(inputfile).readlines()
    
    lower,upper=indices[0].split('..')
    res = {}
    if len(efms):
        for id in range(int(lower),int(upper)):
            #print efms[int(id)].strip('\n')
            res[id] = efms[int(id)].strip('\n')
    else:
        exit(1)
    
    return res
        
def index_type(s):
    try:
        return int(s)
    except ValueError:
        try:
            return map(int, s.split(".."))
        except:
            raise ArgumentTypeError("Invalid index: %s" % (s,))
        
def main():
    parser = argparse.ArgumentParser(description='Selecting some Elementary Flux \
                                     Modes by indexes.' +__author__+ '.',version='1.0')
    parser.add_argument('inputfile', help='give the name of the efms matrix file')
    parser.add_argument('indices', nargs='*', help='give the indexes of the chosen efms')
    parser.add_argument('-grid', help='create a grid of distribution of the reactions in EFMs, give the name of reactions file')
    
    if (len(sys.argv) == 1):
        parser.print_help()
        sys.exit(1)
    else:
        try:
            args = parser.parse_args()
            if args.inputfile is None:
                sys.exit("Invalid file name: %s" % (args.inputfile,))
            else:
                if args.indices is None:
                    res = print_efms_via_ids(inputfile=args.inputfile)
                else:
                    res = print_efms_via_ids(inputfile=args.inputfile,indices=args.indices)
            if args.grid:
                reacts = open(args.grid).readlines()
                reactions = Set([])
                s = '\\documentclass{book}\n\
\\usepackage[margin=2cm,landscape,a3paper]{geometry}%\n\
\\usepackage[table]{xcolor}\n\
\\begin{document}\n'
                s += '\\begin{table}[h!]\n\
\centering\n\
\\footnotesize\n\
\\begin{tabular}{'
                
                cc = '|c|'
                for r in reacts:
                    cc += 'l|'
                cc += '} '
                
                s += cc
                s += ' \hline \n\multicolumn{1}{|p{4mm}|}{~}\n'
                
                for r in reacts:
                    reactions.add(r.strip('\n').strip())
                    s += '& \multicolumn{1}{p{2mm}|}{~}'
                    #s += '\multicolumn{1}{>{\centering}p{5mm}|}{~} &'
                
                s += '\\' +  '\\' + ' \\hline'
                print s #opening \begin{table}...
                ## printing each rows are efms
                keys = res.keys()
                for key in keys:
                    efm = res[key].split()
                    s = 'EFM'+str(key)
                    for r in efm:
                        if r <>'0.0':
                            s += '& \cellcolor[HTML]{F8FF00}'
                        else:
                            s +=' & \cellcolor[HTML]{B2BEB5}'
                            
                    print s + '\\' + '\\'
                
                print '\\hline\n'
                print '\\end{tabular} \n\\end{table} \n\\end{document}'
                    
        except IOError as msg:
            parser.error(str(msg))
            
if __name__ == '__main__':
    main()