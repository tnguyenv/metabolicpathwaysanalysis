#!/usr/bin/python 
"""
Creating a simple graph.
"""
__author__ = """Tung Nguyen Vu Ngoc (nvntung@gmail.com)"""

import sys
import networkx as nx
from networkx import *
import matplotlib.pyplot as plt
import argparse
import csv

def demo_graph_object():
	# Demo using Graph object in networkx package
	'''
	G = nx.Graph()
	G.add_node('A')
	G.add_nodes_from(['B','C'])
	
	print 'The number of nodes is',G.number_of_nodes()
	print 'The number of edges is',G.number_of_edges()
	print G.nodes()
	print G.edges()
	'''
	
	# add some edges
	G.add_edges_from([('A','B'),('A','C'),('B','C')])
	print(G.edges())
	G.add_node('D')
	G.add_edges_from([('A','D'),('B','D')])
	print(G.edges())
	#nx.draw(G)
	#plt.show()
	#plt.savefig("ABCD.png")
	# Tinh bac degree
	print(nx.degree(G))
	G.add_node('E')
	G.add_edge('C','E')
	G.remove_node('D')
	print(G.edges())
	print(nx.degree(G))

def make_statistics_structural_properties(data_file,reactions_removed_file=None):
	edgelist = open(data_file,'r').readlines()
	G2 = nx.Graph()
	for e in edgelist:
		e = e.split()
		G2.add_node(e[0]);
		G2.add_node(e[1]);
		G2.add_edge(e[0],e[1])
	print '===Global network==='
	print 'Number of nodes:',len(G2.nodes())
	print 'Number of edges:',len(edgelist)
	print 'Connected Components: ',len(nx.connected_components(G2))
	print 'Number of clusters:',len(nx.clustering(G2))
	print nx.clustering(G2)
	#print sorted(nx.degree(G2).values())
	#print density(G2)
	#print degree_histogram(G2)

	# drawing the full network
	#nx.draw_spring(G2,node_size=0,edge_color='b',alpha=.2,font_size=10)
	#plt.show()
	#nx.draw(G2)
	#plt.show()
	#plt.savefig("G2.png")
	
	degrees = nx.degree(G2).values()
	print(degrees)
	degrees = nx.degree(G2)
	# Display degrees
	#for deg in degrees:
	#	print deg,":",degrees[deg]
	# Or print purely
	print(degrees)

	# distribution of the degree
	print degree_histogram(G2)
	plt.loglog(degree_histogram(G2),'b-',marker='o')
	plt.title("Degree rank plot")
	plt.ylabel("degree")
	plt.xlabel("rank")
	plt.show()
	#degree_sequence = sorted(nx.degree(G2).values(),reverse=True)
	#print degree_sequence
	#plt.loglog(degree_sequence,'b-',marker='o')
	#plt.show()
	
	###### Working with each of individual matrix Vac_c, Vac_f, etc.
	'''
	Using a loop to run the test for 11 matrices
	'''
	if reactions_removed_file == None:
		sys.exit(1)
	filenames = open(reactions_removed_file,'r').readlines()

	#res = csv.writer(open("statistic_basic_properties.csv", "wb"))
	#res.writerow(["Name","Nb.ConnectedComponents","Nb.Clusters","Density","Periphery","Diameter","Radius","Avg.Degree","Avg.ClusteringCoefficient"])
	# Extract all deleted nodes and edges list to flat file
	res = open("deleted_nodes.txt","wb")
	for fn in filenames:
		print '===',fn.strip('\n'),'==='
		G3 = G2.copy()
		reactions_zero = open(fn.strip('\n'),'r').readline()
		reacts = reactions_zero.split()
		# remove nodes out the started graph. These nodes are contained in the modes_Vss_reactions_zero.txt
		# Cach 1
		#for r in reacts:
		#	G3.remove_node(r)
		# Cach 2
		G3.remove_nodes_from(reacts)
		print 'Connected Components: ',len(nx.connected_components(G3))

		degrees = nx.degree(G3)
		# Display degrees
		#for deg in degrees:
		#	print deg,":",degrees[deg]
		#print(degrees)
		'''
		Removal of some nodes makes a few of node isolated. Consequently the graph isn't still connected.
		So we should remove the nodes which make the graph disconnected before computing anything.
		'''
		# Remove the nodes which degrees are zero
		del_nodes = []
		#print 'Nb. nodes before removing ', len(G3.nodes())
		for deg in degrees:
			if degrees[deg] == 0:
				del_nodes.append(deg)
				G3.remove_node(deg);
		#print 'Nb. nodes after removing ', len(G3.nodes())
		
		# Calculate clustering coefficience
		from networkx.algorithms import bipartite
		#print(bipartite.is_bipartite(G2))
		#print(bipartite.is_bipartite(G3))
		X, Y = bipartite.sets(G3)
		#print list(X)
		#print list(Y)
		#print 'Average clustering (the original graph):',bipartite.average_clustering(G2) 
		#print 'Average clustering (the remained graph after removing some nodes):',bipartite.average_clustering(G3)
		#print '\nAverage Degree Connectivity\n',nx.average_degree_connectivity(G3)
		#print '\nNearest Neighbors\n',nx.k_nearest_neighbors(G3)
		#print '\nAverage Neighbor Degree\n',nx.average_neighbor_degree(G3)
		#print '\nDegree Centrality\n',degree_centrality(G3)
		#print '\nCloseness Centrality\n',closeness_centrality(G3)

		degree_sequence=list(degree(G3).values()) # degree sequence
		
		#print("Degree sequence %s" % degree_sequence)
		#print sum(degree_sequence)
		#print len(degree_sequence)
		#print('%f' % (sum(degree_sequence)/len(degree_sequence)))
		#print("Degree histogram")
		hist={}
		for d in degree_sequence:
			if d in hist:
				hist[d]+=1
			else:
				hist[d]=1
		#print("degree #nodes")
		#for d in hist:
		#	print('%d %d' % (d,hist[d]))
		#print density(G2)
		#print density(G3)
		'''
		print nx.eccentricity(G3)
		print nx.diameter(G3)
		print nx.radius(G3)
		'''
		print 'Connected Components: ',len(nx.connected_components(G3))
		#print sorted(nx.degree(G2).values())
		print 'Number of clusters:',len(nx.clustering(G3))
		
		G = G3.copy()
		#print("eccentricity: %s" % eccentricity(G))
		print("radius: %d" % radius(G))
		print("diameter: %d" % diameter(G))
		print("center: %s" % center(G))
		print("periphery: %s" % periphery(G))
		print("density: %s" % density(G))

		# Save the results
		#res.writerow([fn,len(nx.connected_components(G3)),len(nx.clustering(G3)),density(G),periphery(G),diameter(G),radius(G),sum(degree_sequence)/len(degree_sequence),bipartite.average_clustering(G3)])

		res.write(fn)
		res.write("\t"+reacts.__str__()+"\n")
		res.write("\t"+del_nodes.__str__()+"\n")
		for line in nx.generate_edgelist(G, data=False):
			res.write("\t"+line.__str__()+"\n")
		res.write("\n\n\n")
	res.close()
def main():
	#use this parameter: prog='convert_matrix_to_list_gui.py', usage='%(prog)s [options]'
	parser = argparse.ArgumentParser( 
		description='This script is used to compute a few structural properties of Graph like degree, degree centrality, clustering coefficient, etc., written by '+__author__+'.',version='1.0')
	parser.add_argument('-f','--file',metavar='file-name',help='give the graph data file (.dat, .txt) containing reactions or edge list',required=True,type=file)
	parser.add_argument('-l','--list',metavar='matrix-file',help='give the name of file containing reactions which occurrences are zero',type=file,required=False)
	#parser.add_argument('-file',nargs=3,help='input 3 files: matrix-file reaction-file out-file')
	if len(sys.argv)==1:
	    parser.print_help()
	    sys.exit(1)
	try:
	    args = parser.parse_args() 
	except IOError, msg:
	    parser.error(str(msg))
	if (args.file):
		make_statistics_structural_properties(sys.argv[2])
	if (args.file and args.list):
		make_statistics_structural_properties(sys.argv[2],sys.argv[4])

#./networkx_demo.py -f ~/Dropbox/these/data/fpcBMC-without-subenzymes-edges.txt -l modes_zero_fnames.txt
if __name__ == '__main__':
	main()
