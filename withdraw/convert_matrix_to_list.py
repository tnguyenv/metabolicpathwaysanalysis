#!/usr/bin/python

import sys
import time
import datetime
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
                        FileTransferSpeed, FormatLabel, Percentage, \
                        ProgressBar, ReverseBar, RotatingMarker, \
                        SimpleProgress, Timer
def matrix2list(source_matrix,reactions,target_list):
    widgets=[Percentage(), Bar()]
    pbar = ProgressBar(widgets=widgets, maxval=len(source_matrix)).start()
    k = 0
    bd = time.time()
    for ln in source_matrix:
        ln = ln.split()
        s = ''
        i = 0
        for i in range(len(reactions)):
            if ln[i] != '0.0':
                s += reactions[i] + ' '
        s = s.strip()
        s += '\n'
        target_list.write(s)
        pbar.update(k)
        k += 1
    pbar.finish()
    target_list.close()
    kt = time.time()
    print ">>> Running time: ",kt-bd," seconds = ",str(datetime.timedelta(seconds=kt-bd))
