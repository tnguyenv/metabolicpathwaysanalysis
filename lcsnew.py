#! /usr/bin/python

def lcs(a, b):
    #http://rosettacode.org/wiki/Longest_common_subsequence
    lengths = [[0 for j in range(len(b)+1)] for i in range(len(a)+1)]
    # row 0 and column 0 are initialized to 0 already
    for i, x in enumerate(a):
        for j, y in enumerate(b):
            if x == y:
                lengths[i+1][j+1] = lengths[i][j] + 1
            else:
                lengths[i+1][j+1] = \
                    max(lengths[i+1][j], lengths[i][j+1])
    # read the substring out from the matrix
    result = ""
    x, y = len(a), len(b)
    while x != 0 and y != 0:
        if lengths[x][y] == lengths[x-1][y]:
            x -= 1
        elif lengths[x][y] == lengths[x][y-1]:
            y -= 1
        else:
            assert a[x-1] == b[y-1]
            result = a[x-1] + " " + result
            x -= 1
            y -= 1
    return result.split()

def filter_reactions(s):
    efm = s.strip().split()
    temp_efm = []
    for e in efm:
        if (e[0].isdigit()) or (e[0]=='-'):
            pass
        else:
            temp_efm.append(e)
        
    return temp_efm
'''
s1 = "4: 1 R11i    1 R12    1 R13    1 R14    1 T2    1 T5"
s2 = "7: 1 R6i    -1 R15    -2 T5    1 T6    1 T7    "
'''
s1 = "gln_up_Vgs Vg6pdh_Vepi_Tx5p Vpk_p_Vpdh_p_VFAx_Vdag_Vglyc3P Vut_NRJ3_Vpglm Vsps_Vspace Vfbp Vpk Vcl Vgapdh_p Vrbco Vme Vinv NRJ1 Vala_out Vac_g Vac_f Vpgi Vald Vtpi Vmdh Vpgi_p Vepi_p Tg6p Ttp Vala Vgapdh_Vpgk_Vpgm_Veno Vaco_Vidh Vriso_p_Vtkx_p_Vtald_p"
s2 = "gln_up_Vgs Vg6pdh_Vepi_Tx5p Vpk_p_Vpdh_p_VFAx_Vdag_Vglyc3P Vut_NRJ3_Vpglm Vsps_Vspace Vfbp Vpk Vcl Vgapdh_p Vrbco Vinv NRJ1 Vala_out Vac_g Vac_f Vac_m Vpgi Vald Vtpi Vmdh Vpgi_p Vepi_p Tg6p Ttp Vala Vgapdh_Vpgk_Vpgm_Veno Vaco_Vidh Vriso_p_Vtkx_p_Vtald_p"

print s1
print s2
s1 = filter_reactions(s1)
s2 = filter_reactions(s2)

print lcs(s1,s2)
