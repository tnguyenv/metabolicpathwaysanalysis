#!/usr/bin/python

from __future__ import division
import sys
import time
import datetime
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
                        FileTransferSpeed, FormatLabel, Percentage, \
                        ProgressBar, ReverseBar, RotatingMarker, \
                        SimpleProgress, Timer

## File contains the motif and its list of mcs
fin = open(sys.argv[1]).readlines()
start = time.time()
n = len(fin)
end = time.time()
print ">>> Total running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))
print "The number of lines: ", n
widgets=[Percentage(), Bar()]
pbar = ProgressBar(widgets=widgets,maxval=n).start()
i = 0
## File contains the motif and its size with the number of mcses
fout = open(sys.argv[2],"w")
selected_size = int(sys.argv[3])
j = 0
for line in fin:
    ln = line.split()
    csize = len(ln)
    if csize == selected_size:
        j = j + 1
        fout.write(line)
    else:
        pass
    i = i + 1
    pbar.update(i)
pbar.finish()

end = time.time()
print ">>> Total running time: ",end-start," seconds = ", str(datetime.timedelta(seconds=end-start))
print "The number of mcs longs ", selected_size, " is ", j
fout.close()