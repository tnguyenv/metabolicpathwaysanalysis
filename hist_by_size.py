#!/usr/bin/python
from __future__ import division
import sys
import time
import datetime
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
                        FileTransferSpeed, FormatLabel, Percentage, \
                        ProgressBar, ReverseBar, RotatingMarker, \
                        SimpleProgress, Timer

fn = open(sys.argv[1]).readlines()
summarize = 0.0
n = len(fn)
minimum = 100 
maximum = 0

widgets = [FormatLabel('Processed: %(value)d lines (in: %(elapsed)s)')]
pbar = ProgressBar(widgets=widgets,maxval=n).start()
i = 1
sizehist = {'':[]}
for line in fn:
    line = line.strip()
    cs = line.split()
    temp = len(cs)
    if temp < minimum:
        minimum = temp
    if temp > maximum:
        maximum = temp
    summarize += temp

    if temp in sizehist:
        sizehist[temp].add(i)
    else:
        sizehist[temp] = set()
        sizehist[temp].add(i)
    pbar.update(i)
    i = i + 1
pbar.finish()

del sizehist['']
size = len(sizehist)
if (len(sys.argv)==3):
    fout = open(sys.argv[2],"w")
    for k in sizehist:
        fout.write(str(k) + "\t" + str(len(sizehist[k])) + "\n")
    fout.close()
else:
    print sizehist
print "there is ", size, " groupes"
print "la minimum des longueurs est ", minimum
print "la maximum des longueurs est ", maximum
print "la moyenne des longueurs est ", float(summarize)/n
